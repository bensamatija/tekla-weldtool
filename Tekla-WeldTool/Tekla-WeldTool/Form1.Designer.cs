﻿namespace Tekla_WeldTool
{
    partial class WeldTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeldTool));
            this.CicleWeldType = new System.Windows.Forms.GroupBox();
            this.CicleWeldType_3 = new System.Windows.Forms.Button();
            this.CicleWeldType_8 = new System.Windows.Forms.Button();
            this.CicleWeldType_7 = new System.Windows.Forms.Button();
            this.CicleWeldType_6 = new System.Windows.Forms.Button();
            this.CicleWeldType_5 = new System.Windows.Forms.Button();
            this.CicleWeldType_4 = new System.Windows.Forms.Button();
            this.WeldsOther = new System.Windows.Forms.GroupBox();
            this.Btn_FixWelds = new System.Windows.Forms.Button();
            this.CicleWeldType.SuspendLayout();
            this.WeldsOther.SuspendLayout();
            this.SuspendLayout();
            // 
            // CicleWeldType
            // 
            this.CicleWeldType.Controls.Add(this.CicleWeldType_3);
            this.CicleWeldType.Controls.Add(this.CicleWeldType_8);
            this.CicleWeldType.Controls.Add(this.CicleWeldType_7);
            this.CicleWeldType.Controls.Add(this.CicleWeldType_6);
            this.CicleWeldType.Controls.Add(this.CicleWeldType_5);
            this.CicleWeldType.Controls.Add(this.CicleWeldType_4);
            this.CicleWeldType.Font = new System.Drawing.Font("Consolas", 7.875F);
            this.CicleWeldType.Location = new System.Drawing.Point(12, 12);
            this.CicleWeldType.Name = "CicleWeldType";
            this.CicleWeldType.Size = new System.Drawing.Size(642, 150);
            this.CicleWeldType.TabIndex = 1;
            this.CicleWeldType.TabStop = false;
            this.CicleWeldType.Text = "Cicle Weld Type";
            // 
            // CicleWeldType_3
            // 
            this.CicleWeldType_3.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_3.Location = new System.Drawing.Point(6, 31);
            this.CicleWeldType_3.Name = "CicleWeldType_3";
            this.CicleWeldType_3.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_3.TabIndex = 7;
            this.CicleWeldType_3.Text = "3";
            this.CicleWeldType_3.UseVisualStyleBackColor = true;
            this.CicleWeldType_3.Click += new System.EventHandler(this.CicleWeldType_3_Click);
            // 
            // CicleWeldType_8
            // 
            this.CicleWeldType_8.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_8.Location = new System.Drawing.Point(536, 31);
            this.CicleWeldType_8.Name = "CicleWeldType_8";
            this.CicleWeldType_8.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_8.TabIndex = 6;
            this.CicleWeldType_8.Text = "8";
            this.CicleWeldType_8.UseVisualStyleBackColor = true;
            this.CicleWeldType_8.Click += new System.EventHandler(this.CicleWeldType_8_Click);
            // 
            // CicleWeldType_7
            // 
            this.CicleWeldType_7.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_7.Location = new System.Drawing.Point(430, 31);
            this.CicleWeldType_7.Name = "CicleWeldType_7";
            this.CicleWeldType_7.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_7.TabIndex = 5;
            this.CicleWeldType_7.Text = "7";
            this.CicleWeldType_7.UseVisualStyleBackColor = true;
            this.CicleWeldType_7.Click += new System.EventHandler(this.CicleWeldType_7_Click);
            // 
            // CicleWeldType_6
            // 
            this.CicleWeldType_6.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_6.Location = new System.Drawing.Point(324, 31);
            this.CicleWeldType_6.Name = "CicleWeldType_6";
            this.CicleWeldType_6.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_6.TabIndex = 4;
            this.CicleWeldType_6.Text = "6";
            this.CicleWeldType_6.UseVisualStyleBackColor = true;
            this.CicleWeldType_6.Click += new System.EventHandler(this.CicleWeldType_6_Click);
            // 
            // CicleWeldType_5
            // 
            this.CicleWeldType_5.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_5.Location = new System.Drawing.Point(218, 31);
            this.CicleWeldType_5.Name = "CicleWeldType_5";
            this.CicleWeldType_5.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_5.TabIndex = 3;
            this.CicleWeldType_5.Text = "5";
            this.CicleWeldType_5.UseVisualStyleBackColor = true;
            this.CicleWeldType_5.Click += new System.EventHandler(this.CicleWeldType_5_Click);
            // 
            // CicleWeldType_4
            // 
            this.CicleWeldType_4.Font = new System.Drawing.Font("Consolas", 9F);
            this.CicleWeldType_4.Location = new System.Drawing.Point(112, 31);
            this.CicleWeldType_4.Name = "CicleWeldType_4";
            this.CicleWeldType_4.Size = new System.Drawing.Size(100, 100);
            this.CicleWeldType_4.TabIndex = 2;
            this.CicleWeldType_4.Text = "4";
            this.CicleWeldType_4.UseVisualStyleBackColor = true;
            this.CicleWeldType_4.Click += new System.EventHandler(this.CicleWeldType_4_Click);
            // 
            // WeldsOther
            // 
            this.WeldsOther.Controls.Add(this.Btn_FixWelds);
            this.WeldsOther.Font = new System.Drawing.Font("Consolas", 7.875F);
            this.WeldsOther.Location = new System.Drawing.Point(12, 168);
            this.WeldsOther.Name = "WeldsOther";
            this.WeldsOther.Size = new System.Drawing.Size(642, 150);
            this.WeldsOther.TabIndex = 2;
            this.WeldsOther.TabStop = false;
            this.WeldsOther.Text = "Other settings";
            // 
            // Btn_FixWelds
            // 
            this.Btn_FixWelds.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Btn_FixWelds.Font = new System.Drawing.Font("Consolas", 7.875F);
            this.Btn_FixWelds.Image = ((System.Drawing.Image)(resources.GetObject("Btn_FixWelds.Image")));
            this.Btn_FixWelds.Location = new System.Drawing.Point(6, 31);
            this.Btn_FixWelds.Name = "Btn_FixWelds";
            this.Btn_FixWelds.Size = new System.Drawing.Size(100, 100);
            this.Btn_FixWelds.TabIndex = 1;
            this.Btn_FixWelds.Text = "Fix Welds";
            this.Btn_FixWelds.UseVisualStyleBackColor = true;
            this.Btn_FixWelds.Click += new System.EventHandler(this.Btn_FixWelds_Click);
            // 
            // WeldTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(668, 333);
            this.Controls.Add(this.WeldsOther);
            this.Controls.Add(this.CicleWeldType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "WeldTool";
            this.Text = "WeldTool";
            this.TopMost = true;
            this.CicleWeldType.ResumeLayout(false);
            this.WeldsOther.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox CicleWeldType;
        private System.Windows.Forms.GroupBox WeldsOther;
        private System.Windows.Forms.Button Btn_FixWelds;
        private System.Windows.Forms.Button CicleWeldType_4;
        private System.Windows.Forms.Button CicleWeldType_5;
        private System.Windows.Forms.Button CicleWeldType_8;
        private System.Windows.Forms.Button CicleWeldType_7;
        private System.Windows.Forms.Button CicleWeldType_6;
        private System.Windows.Forms.Button CicleWeldType_3;
    }
}

